FROM ubuntu:24.04
LABEL maintainer="Remo Wenger privat@remowenger.ch"
LABEL Description="Docker image with several debugging tools installed."

RUN \
  apt-get update && \
  apt-get install -y ncat dnsutils openssh-client iotop sysstat htop rsync openssl